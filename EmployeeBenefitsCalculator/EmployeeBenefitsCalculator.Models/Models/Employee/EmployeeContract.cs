using EmployeeBenefitsCalculator.Models.Enums;
using System.Collections.Generic;

namespace Contracts.Models.Employee
{
    public class EmployeeContract : PersonContract, IPersonContract
    {
        public EmployeeContract()
        {
            Dependents = new List<DependentContract>();
        }
        public PersonTypeEnum PersonType => PersonTypeEnum.Employee;
        public List<DependentContract> Dependents { get; set; }
    }
}
