using EmployeeBenefitsCalculator.Models.Enums;
using System;

namespace Contracts.Models.Employee
{
    public interface IPersonContract
  {
    int Id { get; set; }
    string FirstName { get; set; }
    string MiddleName { get; set; }
    string LastName { get; set; }
    PersonTypeEnum PersonType { get; }
  }

  public class PersonContract
  {
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string LastName { get; set; }
  }
}
