using EmployeeBenefitsCalculator.Models.Enums;

namespace Contracts.Models.Employee
{
    public class DependentContract : PersonContract, IPersonContract
  {
    public PersonTypeEnum PersonType => PersonTypeEnum.Dependent;
  }
}
