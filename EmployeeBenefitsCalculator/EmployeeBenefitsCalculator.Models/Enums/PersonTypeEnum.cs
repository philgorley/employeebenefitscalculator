﻿namespace EmployeeBenefitsCalculator.Models.Enums
{
    public enum PersonTypeEnum
    {
        Employee = 1,
        Dependent = 2
    }
}
