import { PersonModel } from './person-model';
import { PersonType } from '../enums/person-type.enum';

export class DependentModel extends PersonModel {
  personType: PersonType.Dependent;
}
