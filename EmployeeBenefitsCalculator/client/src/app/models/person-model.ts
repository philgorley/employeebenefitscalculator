import { PersonType } from '../enums/person-type.enum';

export class PersonModel {
  id: number; // I wish typescript had a guid implementation
  firstName: string;
  middleName: string;
  lastName: string;
}

export interface IPerson {
  id: number; // I wish typescript had a guid implementation
  firstName: string;
  middleName: string;
  lastName: string;
  // personType: PersonType;
}
