import { DependentModel } from './dependent-model';
import { PersonType } from './../enums/person-type.enum';
import { PersonModel } from './person-model';

export class EmployeeModel extends PersonModel {
  personType: PersonType.Employee;
  dependents: DependentModel[];
}
