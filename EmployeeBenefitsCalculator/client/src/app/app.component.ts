import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';
  activeRoute = location.pathname;

  changeActiveRoute (route: string): void {
    this.activeRoute = route;
  }
}
