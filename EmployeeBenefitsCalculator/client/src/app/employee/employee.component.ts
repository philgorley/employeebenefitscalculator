import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { EmployeeHttpService } from './employee-http.service';
import { EmployeeModel } from '../models/employee-model';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  identifier: number;
  form: FormGroup;
  totalBenefitCost = null;

  constructor(private fb: FormBuilder, private service: EmployeeHttpService) { }

  ngOnInit() {
    this.identifier = 0;
    this.form = this.fb.group({
      employees: this.fb.array([
        this.addEmployeeGroup()
      ])
    });
  }

  get employees(): FormArray {
    return this.form.get('employees') as FormArray;
  }

  addEmployee () {
    this.employees.push(this.addEmployeeGroup());
  }

  addDependent (id: number) {
    const found = (this.employees.controls[id] as FormGroup).controls;
    (found.dependents as FormArray).push(this.addDependentGroup());
  }

  removeDependent (id: number, index: number) {
    const found = (this.employees.controls[id] as FormGroup).controls;
    (found.dependents as FormArray).removeAt(index);
  }

  addEmployeeGroup (): FormGroup {
    const employeeGroup = new FormGroup({
      id: new FormControl(this.identifier),
      firstName: new FormControl(`emp${this.identifier}`, Validators.required),
      middleName: new FormControl(''),
      lastName: new FormControl('', Validators.required),
      dependents: this.fb.array([
        this.addDependentGroup(),
      ])
    });
    this.identifier++;
    return employeeGroup;
  }

  addDependentGroup (): FormGroup {
    return new FormGroup({
      id: new FormControl(0),
      firstName: new FormControl('', Validators.required),
      middleName: new FormControl(''),
      lastName: new FormControl('', Validators.required),
    });
  }

  calculateCost (): void {
    const formValue = this.form.getRawValue().employees as Array<EmployeeModel>;
    this.service.calculateBenefitCost(formValue)
      .subscribe(response => this.totalBenefitCost = response);
  }

}
