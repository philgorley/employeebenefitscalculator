import { FormGroup, ReactiveFormsModule, FormArray } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeComponent } from './employee.component';
import { of } from 'rxjs';
import { EmployeeHttpService } from './employee-http.service';
import { EmployeeModel } from '../models/employee-model';

export class EmployeeHttpServiceStub {
  calculateBenefitCost = jasmine.createSpy('calculateBenefitCost').and.returnValue(of({}));
}

describe('EmployeeComponent', () => {
  let component: EmployeeComponent;
  let fixture: ComponentFixture<EmployeeComponent>;
  let employeeHttpServiceStub: EmployeeHttpServiceStub;

  beforeEach(async(() => {
    employeeHttpServiceStub = new EmployeeHttpServiceStub();
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ EmployeeComponent ],
      providers: [ {provide: EmployeeHttpService, useValue: employeeHttpServiceStub} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.form = new FormGroup({
      employees: new FormArray([])
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a method named addDependentGroup that adds a dependent group', () => {
    const result = component.addDependentGroup();
    validatePersonGroup(result);
  });

  it('should have a method named addEmployeeGroup that adds an employee group', () => {
    component.identifier = 0;
    const result = component.addEmployeeGroup();
    validatePersonGroup(result);
    expect(result.controls.dependents).toBeDefined();
    validatePersonGroup((result.controls.dependents as FormArray).controls[0] as FormGroup);
    expect(component.identifier.toString()).toBe((1).toString());
  });

  it('should have a getter to get employees from the form', () => {
    component.addEmployee(); // add in an employee
    component.addEmployee(); // add in another employee
    expect(component.employees.length).toBe(2);
  });

  it('should have a method called addDependent that adds a dependent to an employee', () => {
    component.addEmployee(); // add in an employee
    component.addDependent(0);
    expect(component.employees).toBeDefined();
    expect(((component.employees.controls[0] as FormGroup).controls.dependents as FormGroup).controls[1]).toBeDefined();
    expect(((component.employees.controls[0] as FormGroup).controls.dependents as FormGroup).controls[2]).toBeUndefined();
  });

  it('should have a method called removeDependent that removes a dependent from an employee', () => {
    component.addEmployee(); // add in an employee
    component.removeDependent(0, 0);
    expect(component.employees).toBeDefined();
    expect(((component.employees.controls[0] as FormGroup).controls.dependents as FormGroup).controls[0]).toBeUndefined();
  });

  it('should have a method called calculateCost that calls the service and returns a decimal', () => {
    component.calculateCost();
    const formValue = component.form.getRawValue().employees as Array<EmployeeModel>;
    expect(employeeHttpServiceStub.calculateBenefitCost).toHaveBeenCalledWith(formValue);
  });

  function validatePersonGroup (result: FormGroup): void {
    expect(result).toBeDefined();
    expect(result.controls.id).toBeDefined();
    expect(result.controls.firstName).toBeDefined();
    expect(result.controls.firstName.validator).not.toBeNull();
    expect(result.controls.middleName).toBeDefined();
    expect(result.controls.lastName).toBeDefined();
    expect(result.controls.lastName.validator).not.toBeNull();
  }

});
