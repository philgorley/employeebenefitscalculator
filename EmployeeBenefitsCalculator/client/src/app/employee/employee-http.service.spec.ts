import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { EmployeeHttpService } from './employee-http.service';
import { EmployeeModel } from '../models/employee-model';

describe('EmployeeHttpService', () => {
  let service: EmployeeHttpService;
  let httpMock: HttpTestingController;
  let testBed: TestBed;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EmployeeHttpService]
    });

    testBed = getTestBed();
    service = testBed.get(EmployeeHttpService);
    httpMock = testBed.get(HttpTestingController);

  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have a method to run the calculations', () => {
    expect(service.calculateBenefitCost).toBeDefined();
  });

  it('should make the call to get the calculated value', done => {
    const input = new Array<EmployeeModel>();
    const response = 20231.29;
    service.calculateBenefitCost(input)
      .subscribe(data => {
        expect(data).toEqual(response);
        done();
      });

    const request = httpMock.expectOne('/api/Employee/CalculateBenefitCost');
    expect(request.request.body).toEqual(input);
    expect(request.request.method).toBe('POST');
    request.flush(response);
  });

});
