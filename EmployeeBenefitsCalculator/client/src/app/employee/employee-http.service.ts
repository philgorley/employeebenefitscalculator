import { EmployeeModel } from './../models/employee-model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeHttpService {
  serviceUrls = {
    calculateBenefitCost: '/api/Employee/CalculateBenefitCost',
  };

  constructor(private http: HttpClient) { }

  calculateBenefitCost(input: Array<EmployeeModel>): Observable<number> {
    return this.http.post<number>(this.serviceUrls.calculateBenefitCost, input);
  }
}
