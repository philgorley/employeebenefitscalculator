﻿using System.Collections.Generic;
using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Core.Employee;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeBenefitsCalculator.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private ICalculateEmployeeBenefitCostCommand _calculateEmployeeBenefitCostCommand;

        public EmployeeController(ICalculateEmployeeBenefitCostCommand calculateEmployeeBenefitCostCommand)
        {
            _calculateEmployeeBenefitCostCommand = calculateEmployeeBenefitCostCommand;
        }

        [Route("CalculateBenefitCost")]
        [HttpPost]
        public decimal CalculateBenefitCost(IEnumerable<EmployeeContract> input)
        {
            return _calculateEmployeeBenefitCostCommand.Execute(input);
        }

        [Route("CalculateBenefitCost")]
        [HttpGet]
        public decimal Get()
        {
            return 8562.92m;
        }
    }
}