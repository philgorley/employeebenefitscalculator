﻿using EmployeeBenefitsCalculator.Core.Employee;
using EmployeeBenefitsCalculator.Core.Employee.Calculators;
using Microsoft.Extensions.DependencyInjection;

namespace EmployeeBenefitsCalculator.Service.Di
{
    public class Di
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ICalculateEmployeeBenefitCostCommand, CalculateEmployeeBenefitCostCommand>();
            services.AddSingleton<ICalculatorCommand, CalculatorCommand>();
            services.AddSingleton<ICalculator, EmployeeCalculator>();
            services.AddSingleton<ICalculator, NameStartsWithADiscountCalculator>();
            services.AddSingleton<ICalculator, DependentCalculator>();
        }
    }
}
