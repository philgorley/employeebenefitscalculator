﻿using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Core.Employee.Calculators;
using EmployeeBenefitsCalculator.Models.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EmployeeBenefitsCalculator.Tests.Core.Employee.Calculators
{
    [TestClass]
    public class TestEmployeeCalculator
    {
        private EmployeeCalculator _calculator;
        private EmployeeContract _employeeContract;

        [TestInitialize]
        public void Setup()
        {
            _calculator = new EmployeeCalculator();
            _employeeContract = new EmployeeContract();
        }

        [TestMethod]
        public void TestCalculateReturns1000()
        {
            var result = _calculator.Calculate(_employeeContract);
            Assert.AreEqual(1000, result);
        }

        [TestMethod]
        public void TestIsMatchReturnsTrue()
        {
            var result = _calculator.IsMatch(_employeeContract);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestValidPersonTypesHasCorrectValues()
        {
            var result = _calculator.ValidPersonTypes;
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(result[0], PersonTypeEnum.Employee);
        }
    }
}
