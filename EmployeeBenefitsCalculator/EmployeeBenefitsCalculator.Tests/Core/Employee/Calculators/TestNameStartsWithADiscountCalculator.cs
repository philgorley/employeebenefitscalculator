﻿using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Core.Employee.Calculators;
using EmployeeBenefitsCalculator.Models.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EmployeeBenefitsCalculator.Tests.Core.Employee.Calculators
{
    [TestClass]
    public class TestNameStartsWithADiscountCalculator
    {
        private NameStartsWithADiscountCalculator _calculator;
        private EmployeeContract _employeeContract;

        [TestInitialize]
        public void Setup()
        {
            _calculator = new NameStartsWithADiscountCalculator();
            _employeeContract = new EmployeeContract();
        }

        [TestMethod]
        public void TestCalculateReturns90Percent()
        {
            var result = _calculator.Calculate(_employeeContract);
            Assert.AreEqual(0.9m, result);
        }

        [TestMethod]
        public void TestIsMatchReturnsTrueFirstName()
        {
            _employeeContract.FirstName = "Arthur";
            var result = _calculator.IsMatch(_employeeContract);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestIsMatchReturnsFalseFirstName()
        {
            _employeeContract.FirstName = "Bacon";
            var result = _calculator.IsMatch(_employeeContract);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestIsMatchReturnsTrueMiddleName()
        {
            _employeeContract.MiddleName = "Arthur";
            var result = _calculator.IsMatch(_employeeContract);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestIsMatchReturnsFalseMiddleName()
        {
            _employeeContract.MiddleName = "Bacon";
            var result = _calculator.IsMatch(_employeeContract);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestIsMatchReturnsTrueLastName()
        {
            _employeeContract.LastName = "Arthur";
            var result = _calculator.IsMatch(_employeeContract);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestIsMatchReturnsFalseLastName()
        {
            _employeeContract.LastName = "Bacon";
            var result = _calculator.IsMatch(_employeeContract);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void TestValidPersonTypesHasCorrectValues()
        {
            var result = _calculator.ValidPersonTypes;
            Assert.AreEqual(2, result.Count);
            Assert.IsTrue(result.Contains(PersonTypeEnum.Employee));
            Assert.IsTrue(result.Contains(PersonTypeEnum.Dependent));
        }
    }
}
