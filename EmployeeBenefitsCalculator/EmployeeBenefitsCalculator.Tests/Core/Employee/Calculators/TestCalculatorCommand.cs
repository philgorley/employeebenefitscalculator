﻿using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Core.Employee.Calculators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace EmployeeBenefitsCalculator.Tests.Core.Employee.Calculators
{
    [TestClass]
    public class TestCalculatorCommand
    {
        private CalculatorCommand _command;
        private List<ICalculator> _calculatorList;
        private EmployeeContract _employeeContract;
        private Mock<EmployeeCalculator> _mockEmployeeCalculator;
        private Mock<DependentCalculator> _mockDependentCalculator;
        private Mock<NameStartsWithADiscountCalculator> _mockDiscountCalculator;

        [TestInitialize]
        public void Setup()
        {
            _mockEmployeeCalculator = new Mock<EmployeeCalculator>();
            _mockDependentCalculator = new Mock<DependentCalculator>();
            _mockDiscountCalculator = new Mock<NameStartsWithADiscountCalculator>();
            _calculatorList = new List<ICalculator>
            {
                _mockEmployeeCalculator.Object,
                _mockDependentCalculator.Object,
                _mockDiscountCalculator.Object
            };
            _command = new CalculatorCommand(_calculatorList);
            _employeeContract = new EmployeeContract();

            _mockEmployeeCalculator.Setup(x => x.IsMatch(It.IsAny<IPersonContract>())).Returns(true);
            _mockEmployeeCalculator.Setup(x => x.Calculate(It.IsAny<IPersonContract>())).Returns(1000m);

            _mockDependentCalculator.Setup(x => x.IsMatch(It.IsAny<IPersonContract>())).Returns(false);
            _mockDependentCalculator.Setup(x => x.Calculate(It.IsAny<IPersonContract>())).Returns(2m);

            _mockDiscountCalculator.Setup(x => x.IsMatch(It.IsAny<IPersonContract>())).Returns(true);
            _mockDiscountCalculator.Setup(x => x.Calculate(It.IsAny<IPersonContract>())).Returns(0.2m);

        }

        [TestMethod]
        public void TestExecute()
        {
            var result = _command.Execute(_employeeContract);
            _mockEmployeeCalculator.Verify(x => x.Calculate(_employeeContract), Times.Once);
            _mockEmployeeCalculator.Verify(x => x.IsMatch(_employeeContract), Times.Once);

            _mockDependentCalculator.Verify(x => x.IsMatch(_employeeContract), Times.Never);
            _mockDependentCalculator.Verify(x => x.Calculate(_employeeContract), Times.Never);

            _mockDiscountCalculator.Verify(x => x.IsMatch(_employeeContract), Times.Once);
            _mockDiscountCalculator.Verify(x => x.Calculate(_employeeContract), Times.Once);

            Assert.AreEqual(200m, result);
        }
    }
}
