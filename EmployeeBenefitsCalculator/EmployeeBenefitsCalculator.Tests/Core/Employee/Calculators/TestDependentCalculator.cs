﻿using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Core.Employee.Calculators;
using EmployeeBenefitsCalculator.Models.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EmployeeBenefitsCalculator.Tests.Core.Employee.Calculators
{
    [TestClass]
    public class TestDependentCalculator
    {
        private DependentCalculator _calculator;
        private DependentContract _dependentContract;

        [TestInitialize]
        public void Setup()
        {
            _calculator = new DependentCalculator();
            _dependentContract = new DependentContract();
        }

        [TestMethod]
        public void TestCalculateReturns500()
        {
            var result = _calculator.Calculate(_dependentContract);
            Assert.AreEqual(500, result);
        }

        [TestMethod]
        public void TestIsMatchReturnsTrue()
        {
            var result = _calculator.IsMatch(_dependentContract);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void TestValidPersonTypesHasCorrectValues()
        {
            var result = _calculator.ValidPersonTypes;
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(result[0], PersonTypeEnum.Dependent);
        }

    }
}
