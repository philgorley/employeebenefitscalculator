﻿using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Core.Employee;
using EmployeeBenefitsCalculator.Core.Employee.Calculators;
using EmployeeBenefitsCalculator.Tests.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace EmployeeBenefitsCalculator.Tests.Core.Employee
{
    [TestClass]
    public class TestCalculateEmployeeBenefitCostCommand
    {
        private CalculateEmployeeBenefitCostCommand _command;
        private Mock<ICalculatorCommand> _mockCalculatorCommand;
        private List<EmployeeContract> _contracts;

        [TestInitialize]
        public void Setup()
        {
            _mockCalculatorCommand = new Mock<ICalculatorCommand>();
            _command = new CalculateEmployeeBenefitCostCommand(_mockCalculatorCommand.Object);
            _contracts = EmployeeContractData.EmployeesList();
            _mockCalculatorCommand.Setup(x => x.Execute(It.IsAny<IPersonContract>())).Returns(123m);
        }

        [TestMethod]
        public void TestExecuteMethod()
        {
            var result = _command.Execute(_contracts);
            foreach (var employee in _contracts)
            {
                _mockCalculatorCommand.Verify(x => x.Execute(employee), Times.Once);
                foreach (var dependent in employee.Dependents)
                {
                    _mockCalculatorCommand.Verify(x => x.Execute(dependent), Times.Once);
                }
            }
            Assert.AreEqual(738m, result);
        }
    }
}
