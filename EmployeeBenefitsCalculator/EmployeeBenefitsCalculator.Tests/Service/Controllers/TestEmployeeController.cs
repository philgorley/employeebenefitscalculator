﻿using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Core.Employee;
using EmployeeBenefitsCalculator.Service.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace EmployeeBenefitsCalculator.Tests.Service.Controllers
{
    [TestClass]
    public class TestEmployeeController
    {
        private EmployeeController _controller;
        private Mock<ICalculateEmployeeBenefitCostCommand> _mockCalculateEmployeeBenefitCostCommand;
        private IEnumerable<EmployeeContract> _employeeContracts;
        
        [TestInitialize]
        public void Setup()
        {
            _mockCalculateEmployeeBenefitCostCommand = new Mock<ICalculateEmployeeBenefitCostCommand>();
            _controller = new EmployeeController(_mockCalculateEmployeeBenefitCostCommand.Object);
            _employeeContracts = new List<EmployeeContract>()
            {
                new EmployeeContract()
            };
            _mockCalculateEmployeeBenefitCostCommand.Setup(x => x.Execute(It.IsAny<IEnumerable<EmployeeContract>>())).Returns(1234.56m);
        }

        [TestMethod]
        public void CalculateBenefitCostCallsCalculateBenefitCostQuery_Execute()
        {
            _controller.CalculateBenefitCost(_employeeContracts);
            _mockCalculateEmployeeBenefitCostCommand.Verify(x => x.Execute(_employeeContracts), Times.Once);
        }

    }
}
