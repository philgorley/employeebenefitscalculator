﻿using Contracts.Models.Employee;
using System.Collections.Generic;

namespace EmployeeBenefitsCalculator.Tests.Data
{
    public static class EmployeeContractData
    {
        public static EmployeeContract Employee1NoDependents()
        {
            return new EmployeeContract
            {
                FirstName = "Tony",
                LastName = "Bologna",
                Id = 5484
            };
        }

        public static EmployeeContract Employee2NoDependents()
        {
            return new EmployeeContract
            {
                FirstName = "Anthony",
                LastName = "Bologna",
                Id = 4432
            };
        }

        public static EmployeeContract Employee3Dependents()
        {
            return new EmployeeContract
            {
                FirstName = "Justin",
                LastName = "Groober",
                Id = 1123,
                Dependents = DependentContractData.DependentList()
            };
        }

        public static List<EmployeeContract> EmployeesList()
        {
            return new List<EmployeeContract>()
            {
                Employee1NoDependents(),
                Employee2NoDependents(),
                Employee3Dependents()
            };
        }
    }
}
