﻿using Contracts.Models.Employee;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeBenefitsCalculator.Tests.Data
{
    public static class DependentContractData
    {
        public static DependentContract Dependent1()
        {
            return new DependentContract
            {
                FirstName = "Carl",
                LastName = "Sagan",
                Id = 59544
            };
        }

        public static DependentContract Dependent2()
        {
            return new DependentContract
            {
                FirstName = "Neil",
                LastName = "DeGrasse Tyson",
                Id = 532144
            };
        }

        public static DependentContract Dependent3()
        {
            return new DependentContract
            {
                FirstName = "Bill",
                LastName = "Nye (The Science Guy)",
                Id = 1129
            };
        }

        public static List<DependentContract> DependentList()
        {
            return new List<DependentContract>()
            {
                Dependent1(),
                Dependent2(),
                Dependent3()
            };
        }
    }
}
