﻿namespace EmployeeBenefitsCalculator.Core.Common
{
    public interface ICommand<T1>
    {
        T1 Execute();
    }
    
    public interface ICommand<T1, T2>
    {
        T1 Execute(T2 input);
    }
}
