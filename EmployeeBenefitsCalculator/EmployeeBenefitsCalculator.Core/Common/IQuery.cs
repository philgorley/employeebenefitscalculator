﻿namespace EmployeeBenefitsCalculator.Core.Common
{
    public interface IQuery<T1>
    {
        T1 Run();
    }

    public interface IQuery<T1, T2>
    {
        T1 Run(T2 input);
    }
}
