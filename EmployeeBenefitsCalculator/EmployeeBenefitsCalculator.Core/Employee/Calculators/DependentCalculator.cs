﻿using System.Collections.Generic;
using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Models.Enums;

namespace EmployeeBenefitsCalculator.Core.Employee.Calculators
{
    public class DependentCalculator : ICalculator
    {
        public List<PersonTypeEnum> ValidPersonTypes => new List<PersonTypeEnum>() { PersonTypeEnum.Dependent };

        public virtual decimal Calculate(IPersonContract person)
        {
            return 500m;
        }

        public virtual bool IsMatch(IPersonContract person) => true;
    }
}
