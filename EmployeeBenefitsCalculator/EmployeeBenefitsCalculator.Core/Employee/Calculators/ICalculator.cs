﻿using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Models.Enums;
using System.Collections.Generic;

namespace EmployeeBenefitsCalculator.Core.Employee.Calculators
{
    public interface ICalculator
    {
        List<PersonTypeEnum> ValidPersonTypes { get; }
        bool IsMatch(IPersonContract person);
        decimal Calculate(IPersonContract person);
    }
}
