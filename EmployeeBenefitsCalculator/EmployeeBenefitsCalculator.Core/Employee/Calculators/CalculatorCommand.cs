﻿using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Core.Common;
using System.Collections.Generic;
using System.Linq;

namespace EmployeeBenefitsCalculator.Core.Employee.Calculators
{
    public interface ICalculatorCommand : ICommand<decimal, IPersonContract> { }

    public class CalculatorCommand : ICalculatorCommand
    {
        private readonly IEnumerable<ICalculator> _calculators;

        public CalculatorCommand(IEnumerable<ICalculator> calculators)
        {
            _calculators = calculators;
        }

        public decimal Execute(IPersonContract input)
        {
            var calculators = _calculators.Where(x => x.ValidPersonTypes.Contains(input.PersonType) && x.IsMatch(input));
            decimal result = 0m;
            foreach(var calculator in calculators)
            {
                var calculatedResult = calculator.Calculate(input);
                switch (result)
                {
                    case decimal.Zero:
                        result = calculatedResult;
                        break;
                    default:
                        result *= calculatedResult;
                        break;
                }

            }
            return result;
        }
    }
}
