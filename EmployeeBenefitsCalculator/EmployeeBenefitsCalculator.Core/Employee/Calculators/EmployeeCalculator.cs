﻿using System.Collections.Generic;
using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Models.Enums;

namespace EmployeeBenefitsCalculator.Core.Employee.Calculators
{
    public class EmployeeCalculator : ICalculator
    {
        public List<PersonTypeEnum> ValidPersonTypes => new List<PersonTypeEnum>() { PersonTypeEnum.Employee };

        public virtual bool IsMatch(IPersonContract person) => true;

        public virtual decimal Calculate(IPersonContract person)
        {
            return 1000m;
        }
    }
}
