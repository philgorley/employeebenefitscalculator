﻿using System.Collections.Generic;
using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Models.Enums;

namespace EmployeeBenefitsCalculator.Core.Employee.Calculators
{
    public class NameStartsWithADiscountCalculator : ICalculator
    {
        public List<PersonTypeEnum> ValidPersonTypes => new List<PersonTypeEnum>() { PersonTypeEnum.Dependent, PersonTypeEnum.Employee };

        public virtual decimal Calculate(IPersonContract person)
        {
            return 0.9m;
        }

        public virtual bool IsMatch(IPersonContract person)
        {
            if (person.FirstName != null && person.FirstName.ToLower().StartsWith('a')) return true;
            if (person.MiddleName != null && person.MiddleName.ToLower().StartsWith('a')) return true;
            if (person.LastName != null && person.LastName.ToLower().StartsWith('a')) return true;
            return false;
        }
    }
}
