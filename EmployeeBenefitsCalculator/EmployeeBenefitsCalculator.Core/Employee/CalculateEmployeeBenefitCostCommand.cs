﻿using Contracts.Models.Employee;
using EmployeeBenefitsCalculator.Core.Common;
using EmployeeBenefitsCalculator.Core.Employee.Calculators;
using System.Collections.Generic;

namespace EmployeeBenefitsCalculator.Core.Employee
{
    public interface ICalculateEmployeeBenefitCostCommand : ICommand<decimal, IEnumerable<EmployeeContract>> { }

    public class CalculateEmployeeBenefitCostCommand : ICalculateEmployeeBenefitCostCommand
    {
        private readonly ICalculatorCommand _calculatorCommand;

        public CalculateEmployeeBenefitCostCommand(ICalculatorCommand calculatorCommand)
        {
            _calculatorCommand = calculatorCommand;
        }

        public decimal Execute(IEnumerable<EmployeeContract> input)
        {
            decimal result = 0m;
            foreach(var employee in input)
            {
                result += _calculatorCommand.Execute(employee);
                foreach(var dependent in employee.Dependents)
                {
                    result += _calculatorCommand.Execute(dependent);
                }
            }
            return result;
        }
    }
}
